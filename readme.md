# BAKO
## Gestion de planning

Bako et une application de gestion de planning qui permet à l'utilisateur de compter ses heures de travail et de congès


## Fonctionnalités implémentées
- Les différentes vues (Home, Planning, Alarme)
- Gestion de planning
- Gestion des codes d'affectations
- Configurations des codes de travail
- Thème claire/sombre


## Fonctionnalités à faire
- Liaison de l'application avec Strapi
- Gestion des utilisateurs
- Implémentation des codes de dongès
- Vue du planning par semaine et mois


## Accès à la BDD distante (Strapi)
URL: bako.app
Id: jeantetjoey.pro@gmail.com
Mdp: PjnRfoib6i9YmL!i